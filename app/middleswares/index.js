const jwt = require('jsonwebtoken')

exports.hasRole = (role) => {

}

exports.hasPermission = (permission) => {

}

exports.authJwt = (req, res, next) => {
    // Header names in Express are auto-converted to lowercase
    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (!token) return res.status(400).json({
        message: 'Headers not found'
    });
    // Remove Bearer from string
    token = token.replace(/^Bearer\s+/, "");
    if (token) {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        if (!decoded) {
            return res.status(401).json({ message: 'Token is not valid' })
        }
        req.user = decoded;
        next();
    } else {
        return res.status(400).json({
            message: 'Token not found'
        });
    }
}