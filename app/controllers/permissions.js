const { Permission } = require("../models");

// Display list of all Permissions.
exports.list = async (req, res) => {
    try {
        const permissions = await Permission.find();
        return res.status(200).json({ message: 'list request', data: { permissions } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }

};

// Display detail page for a specific Permission.
exports.detail = async (req, res) => {
    try {
        const permission = await Permission.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { permission } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle Permission create on POST.
exports.create = async (req, res) => {
    try {
        const permission = new Permission(req.params);
        await permission.save();
        return res.status(200).json({ message: 'list request', data: { permission } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle Permission update on PUT.
exports.update = async (req, res) => {
    try {
        const result = await Permission.updateOne({ _id: req.params.id }, req.body);
        const permission = await Permission.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { permission } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Display Permission remove form on DELETE.
exports.remove = async (req, res) => {
    try {
        const deleted = await Permission.deleteOne({ _id: req.params.id });
        return res.status(200).json({ message: 'list request', data: { deleted } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};