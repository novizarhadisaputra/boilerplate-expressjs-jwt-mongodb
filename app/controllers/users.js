const { User } = require("../models");

// Display list of all Users.
exports.list = async (req, res) => {
    try {
        const users = await User.find().populate('role');
        return res.status(200).json({ message: 'list request', data: { users } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }

};

// Display detail page for a specific User.
exports.detail = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { user } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle User create on POST.
exports.create = async (req, res) => {
    try {
        const user = new User(req.params.id);
        await user.save();
        return res.status(200).json({ message: 'list request', data: { user } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle User update on PUT.
exports.update = async (req, res) => {
    try {
        const result = await User.updateOne({ _id: req.params.id }, req.body);
        const user = await User.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { user } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Display User remove form on DELETE.
exports.remove = async (req, res) => {
    try {
        const deleted = await User.deleteOne({ _id: req.params.id });
        return res.status(200).json({ message: 'list request', data: { deleted } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};