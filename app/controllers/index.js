const asuransiKebakaran = require("./asuransiKebakaran");
const users = require('./users');
const auth = require('./auth');
const role = require('./roles');
const permission = require('./permissions');

exports.asuransiKebakaranController = asuransiKebakaran;
exports.userController = users;
exports.authController = auth;
exports.roleController = role;
exports.permissionController = permission;