const { mongoose } = require("../../config/database");
const { AsuransiKebakaran } = require("../models");
const { validationResult } = require('express-validator');


// Display list of all AsuransiKebakarans.
exports.list = async (req, res) => {
    try {
        const requestAsuransi = await AsuransiKebakaran.find();
        return res.status(200).json({ message: 'list request', data: { requestAsuransi } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }

};

// Display detail page for a specific AsuransiKebakaran.
exports.detail = async (req, res) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).json({ message: 'ID invalid' })
        const requestAsuransi = await AsuransiKebakaran.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { requestAsuransi } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle AsuransiKebakaran create on POST.
exports.create = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Invalid input', data: errors.array() });
        }
        const requestAsuransi = new AsuransiKebakaran(req.body);
        await requestAsuransi.save();
        return res.status(200).json({ message: 'list request', data: { requestAsuransi } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle AsuransiKebakaran update on PUT.
exports.update = async (req, res) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).json({ message: 'ID invalid' })
        const result = await AsuransiKebakaran.updateOne({ _id: req.params.id }, req.body);
        const requestAsuransi = await AsuransiKebakaran.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { requestAsuransi } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Display AsuransiKebakaran remove form on DELETE.
exports.remove = async (req, res) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).json({ message: 'ID invalid' })
        const deleted = await AsuransiKebakaran.deleteOne({ _id: req.params.id });
        return res.status(200).json({ message: 'list request', data: { deleted } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

exports.accepted = async (req, res) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).json({ message: 'ID invalid' })
        const requestAsuransi = await AsuransiKebakaran.updateOne({ _id: req.params.id }, { accepted: true });
        return res.status(200).json({ message: 'list request', data: { requestAsuransi } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
}

exports.rejected = async (req, res) => {
    try {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).json({ message: 'ID invalid' })
        const requestAsuransi = await AsuransiKebakaran.updateOne({ _id: req.params.id }, { accepted: false });
        return res.status(200).json({ message: 'list request', data: { requestAsuransi } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
}