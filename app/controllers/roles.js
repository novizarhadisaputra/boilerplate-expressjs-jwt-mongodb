const { Role } = require("../models");

// Display list of all Roles.
exports.list = async (req, res) => {
    try {
        const roles = await Role.find();
        return res.status(200).json({ message: 'list request', data: { roles } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }

};

// Display detail page for a specific Role.
exports.detail = async (req, res) => {
    try {
        const role = await Role.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { role } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle Role create on POST.
exports.create = async (req, res) => {
    try {
        const role = new Role(req.body);
        await role.save();
        return res.status(200).json({ message: 'list request', data: { role } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Handle Role update on PUT.
exports.update = async (req, res) => {
    try {
        const result = await Role.updateOne({ _id: req.params.id }, req.body);
        const role = await Role.findById(req.params.id);
        return res.status(200).json({ message: 'list request', data: { role } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

// Display Role remove form on DELETE.
exports.remove = async (req, res) => {
    try {
        const deleted = await Role.deleteOne({ _id: req.params.id });
        return res.status(200).json({ message: 'list request', data: { deleted } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};