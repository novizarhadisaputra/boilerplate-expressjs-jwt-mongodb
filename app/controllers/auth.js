const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const { User } = require("../models");

exports.register = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Invalid input', data: errors.array() });
        }
        const user = new User(req.body);
        await user.save();
        return res.status(200).json({ message: 'list request', data: { user } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

exports.login = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ message: 'Invalid input', data: errors.array() });
        }
        const user = await User.findOne({ email: req.body.email });
        const token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + (process.env.JWT_EXPIRED * 60),
            data: { user: { id: user._id } }
        }, process.env.JWT_SECRET);
        return res.status(200).json({ message: 'list request', data: { user, token } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};

exports.logout = async (req, res) => {
    try {
        const user = await User.findOne(req.body);
        return res.status(200).json({ message: 'list request', data: { user } })
    } catch (error) {
        return res.status(400).json({ message: error.message, data: error.stack })
    }
};
