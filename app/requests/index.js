const requestAK = require('./akCreate');
const requestRegister = require('./authRegister')
const requestLogin = require('./authLogin')

module.exports = { requestAK, requestRegister, requestLogin }