const { body } = require('express-validator');
const validator = [
    body('jangkaWaktuPertanggungan').isNumeric(),
    body('okupasi').isString(),
    body('hargaBangunan').isNumeric(),
    body('konstruksi').isNumeric(),
    body('alamat').isString(),
    body('provinsi').isString(),
    body('kota').isString(),
    body('daerah').isString(),
    // body('gempaBumi'), optional
    // body('accepted'), approval by admin
]
module.exports = validator