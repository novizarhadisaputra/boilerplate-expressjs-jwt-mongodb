const { body } = require('express-validator');
const validator = [
    body('firstName').notEmpty().isString(),
    body('lastName').notEmpty().isString(),
    body('email').isEmail().notEmpty().isString(),
    body('password').notEmpty(),
]
module.exports = validator