const { body } = require('express-validator');
const { User } = require('../models');
let errors = new Error();
const bcrypt = require('bcryptjs')
const validator = [
    body('email').isEmail().notEmpty().isString(),
    body('password').notEmpty().custom(async (value, { req }) => {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            errors.message = 'User is not found :)';
            throw errors;
        }
        const isPasswordMatch = bcrypt.compareSync(req.body.password, user.password);
        if (!isPasswordMatch) {
            errors.message = 'Password not match :)';
            throw errors;
        }
        return true
    })
]
module.exports = validator