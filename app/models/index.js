const asuransiKebakaran = require("./asuransiKebakaran");
const user = require('./user');
const role = require('./role');
const permission = require('./permission');

exports.AsuransiKebakaran = asuransiKebakaran;
exports.User = user;
exports.Role = role;
exports.Permission = permission;