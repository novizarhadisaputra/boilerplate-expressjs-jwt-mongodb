const { mongoose } = require("../../config/database");
const { Schema } = mongoose;

const permissionSchema = new Schema({
    name: { required: true, type: String }
});

module.exports = mongoose.model('Permission', permissionSchema);