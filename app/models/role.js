const { mongoose } = require("../../config/database");
const { Schema } = mongoose;

const roleSchema = new Schema({
    name: { required: true, type: String },
    permissions: [{ required: true, type: Schema.Types.ObjectId, ref: 'Permission' }],
});

module.exports = mongoose.model('Role', roleSchema);