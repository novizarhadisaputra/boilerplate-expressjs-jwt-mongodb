const bcrypt = require('bcryptjs');

const { mongoose } = require("../../config/database");
const { Schema } = mongoose;

const userSchema = new Schema({
    firstName: { required: true, type: String },
    lastName: { required: true, type: String },
    email: { required: true, type: String },
    password: { required: true, type: String },
    role: { required: true, type: Schema.Types.ObjectId, ref: 'Role' },
    verified: { default: false, type: Boolean }
});

userSchema.pre('save', function (next) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(this.password, salt);
    this.password = hash;
    next();
})

module.exports = mongoose.model('User', userSchema);