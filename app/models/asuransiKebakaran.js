const { mongoose } = require("../../config/database");
const { Schema } = mongoose;

const asuransiKebakaranSchema = new Schema({
    jangkaWaktuPertanggungan: { required: true, type: Number },
    okupasi: { required: true, type: String },
    hargaBangunan: { required: true, type: Number },
    konstruksi: { required: true, type: Number },
    alamat: { required: true, type: String },
    provinsi: { required: true, type: String },
    kota: { required: true, type: String },
    daerah: { required: true, type: String },
    gempaBumi: { default: false, type: Boolean },
    accepted: { default: false, type: Boolean }
});

module.exports = mongoose.model('AsuransiKebakaran', asuransiKebakaranSchema);