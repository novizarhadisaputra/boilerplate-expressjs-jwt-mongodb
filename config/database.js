const mongoose = require('mongoose');
function check() {
	mongoose.connect(
		`mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASS}@${process.env.MONGODB_HOST}/${process.env
			.MONGODB_NAME}`,
		{ useNewUrlParser: true, useUnifiedTopology: true }
	);
	mongoose.connection.on('error', () => console.error.bind(console, 'connection error:'));
	mongoose.connection.once('open', () => console.log(`MongoDB connected`));
}
module.exports = { mongoose, check };
