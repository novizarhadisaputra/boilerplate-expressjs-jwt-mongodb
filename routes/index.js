const express = require('express');
const app = express();

const asuransiKebakaranRouter = require('./asuransiKebakaran');
const authRouter = require('./auth')
const roleRouter = require('./role');
const userRouter = require('./user');
const permissionRouter = require('./permission')

app.use('/asuransi-kebakaran', asuransiKebakaranRouter);
app.use('/auth', authRouter);
app.use('/role', roleRouter);
app.use('/permission', permissionRouter);
app.use('/user', userRouter);

module.exports = app;
