const express = require('express');
const { authController } = require('../app/controllers');
const { requestRegister, requestLogin } = require('../app/requests')
const router = express.Router();

router.post('/login', requestLogin, authController.login);
router.post('/register', requestRegister, authController.register);
router.post('/logout', authController.logout);

module.exports = router;
