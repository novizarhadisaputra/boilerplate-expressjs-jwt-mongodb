const express = require('express');
const { roleController } = require('../app/controllers');
const router = express.Router();

router.get('/', roleController.list);
router.get('/:id', roleController.detail);
router.post('/', roleController.create);
router.put('/:id', roleController.update);
router.delete('/:id', roleController.remove);

module.exports = router;
