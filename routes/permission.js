const express = require('express');
const { permissionController } = require('../app/controllers');
const router = express.Router();

router.get('/', permissionController.list);
router.get('/:id', permissionController.detail);
router.post('/', permissionController.create);
router.put('/:id', permissionController.update);
router.delete('/:id', permissionController.remove);

module.exports = router;
