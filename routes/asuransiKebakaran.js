const express = require('express');
const { asuransiKebakaranController } = require('../app/controllers');
const { authJwt } = require('../app/middleswares');
const { requestAK } = require('../app/requests');
const router = express.Router();

router.get('/', authJwt, asuransiKebakaranController.list);
router.get('/:id', asuransiKebakaranController.detail);
router.post('/', [authJwt, requestAK], asuransiKebakaranController.create);
router.put('/:id', asuransiKebakaranController.update);
router.delete('/:id', asuransiKebakaranController.remove);

module.exports = router;
