const { Role, Permission, User } = require('../app/models')

exports.filterRole = async (query) => {
    return await Role.find(query);
}

exports.filterPermission = async (query) => {
    return await Permission.find(query);
}

exports.filterUser = async (query) => {
    return await User.find(query);
}